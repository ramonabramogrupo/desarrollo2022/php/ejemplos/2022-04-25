<?php
    // creamos una variable de tipo array
    $variable=[
        "valores" => ["santander","laredo","potes"],
        "indices" => [0,23,45],
    ];
    
    // creamos una constante
    // colocar el nombre en mayusculas
    define("BOTON", "ENVIAR");
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="get">
            <select name="poblaciones">
                <?php
                echo '<option value="' 
                    . $variable["indices"][0] 
                    . '">' 
                    .  $variable["valores"][0]
                    . '</option>';
                ?>
                <option value="<?= $variable["indices"][1] ?>">
                <?= $variable["valores"][1] ?>
                </option>
                
                <option value="<?= $variable["indices"][2] ?>">
                <?= $variable["valores"][2] ?>
                </option>
            </select>
            <button><?= BOTON ?></button>
        </form>
        <?php
        
        ?>
    </body>
</html>
