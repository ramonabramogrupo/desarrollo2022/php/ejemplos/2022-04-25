<?php
$cajas = ["Parrafo 1", "Parrafo 2", "Parrafo 3"];
$etiquetas = ["codigo", "modelo", "marca"];
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>

        <?php
        for ($c = 0; $c < count($cajas); $c++) {
            echo "<div>$cajas[$c]</div>";
        }
        ?>

        <form method="get">
            <?php
            for ($c = 0; $c < count($etiquetas); $c++) {
                // principio bucle para imprimir los controles del formulario
                ?>
                <label for="<?= $etiquetas[$c] ?>"><?= $etiquetas[$c] ?></label>
                <input type="text" id="<?= $etiquetas[$c] ?>" name="<?= $etiquetas[$c] ?>">
                <?php
                // fin del bucle para imprimir los controles del formulario
            }
            ?>
            <button>Enviar</button>
        </form>

        <?php
        ?>
    </body>
</html>
