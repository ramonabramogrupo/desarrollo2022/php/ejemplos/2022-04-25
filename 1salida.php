<?php
    // creamos una variable de tipo array
    $variable=[
        "valores" => ["santander","laredo","potes"],
        "indices" => [0,23,45],
    ];
    
    // creamos una constante
    // colocar el nombre en mayusculas
    define("BOTON", "ENVIAR");
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="get">
            <select name="poblaciones">
                <?php
                for ($c=0;$c<3;$c++){
                ?>
                <option value="<?= $variable["indices"][$c] ?>">
                <?= $variable["valores"][$c] ?>
                </option> 
                <?php
                }
                ?>
            </select>
            <button><?= BOTON ?></button>
        </form>
        <?php
        
        ?>
    </body>
</html>
